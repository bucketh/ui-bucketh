import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import checker from 'vite-plugin-checker';

const isDev = !!process.env.CHECK || process.env.HISTOIRE === 'true';

export default defineConfig({
	plugins: [
		sveltekit(),
		isDev && checker({
			typescript: true,
		}),
	],
});
