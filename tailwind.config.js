import { buckethPlugin } from './src/lib/plugin';

/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './src/**/*.{html,svelte}'
  ],
  theme: {
    extend: {},
  },
  plugins: [
    buckethPlugin(),
  ],
}
