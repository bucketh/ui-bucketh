import { defineConfig } from 'histoire';
import { HstSvelte } from '@histoire/plugin-svelte';

export default defineConfig({
  vite: {
    server: {
      host: '0.0.0.0',
    },
  },
  plugins: [
    HstSvelte(),
  ],
});
