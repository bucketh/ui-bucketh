export type MenuPosition =
    | 'top-left' | 'top-center' | 'top-right'
    | 'right-top' | 'right-center' | 'right-bottom'
    | 'bottom-right' | 'bottom-center' | 'bottom-left'
    | 'left-bottom' | 'left-center' | 'left-top'
;

export type MenuPositionConfig = MenuPosition | 'auto';
