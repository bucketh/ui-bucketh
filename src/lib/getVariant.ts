export interface GetVariantOptions<Variants extends Record<string, boolean>> {
  variant?: keyof Variants;
  flags: Variants;
  default: keyof Variants;
}

export default function getVariant<Variants extends Record<string, boolean>>(
  options: GetVariantOptions<Variants>,
): keyof Variants {
  if (options.variant) return options.variant;

  const entries: Array<[keyof Variants, boolean]> = Object.entries(options.flags);
  const flag = entries.find(([, isOn]) => isOn);
  if (flag) return flag[0];

  return options.default;
}
