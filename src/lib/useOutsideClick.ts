import { onDestroy } from 'svelte';

/**
 * @deprecated use `createOutsideClick` instead
 */
export default function useOutsideClick(
  callback: (event: MouseEvent) => void
) {
  const elements: Array<HTMLElement> = [];
  const handleClick = (event: MouseEvent) => {
    const isEmpty = elements.length === 0;
    const isInside = elements.some((element) => element?.contains(event.target as Node));
    if (isEmpty || isInside) return;

    callback(event);
  };

  document.addEventListener("click", handleClick);
  onDestroy(() => {
    document.removeEventListener("click", handleClick)
  });

  return function setElements(...newElements: Array<HTMLElement | undefined>) {
    elements.length = 0;
    elements.push(...newElements.filter(Boolean) as Array<HTMLElement>);
  };
}
