export default function createOutsideClick(onClick: (event: MouseEvent) => void) {
  function outsideClick<Element extends HTMLElement>(element: Element) {
    const handleClick = (event: MouseEvent) => {
      if (!element.contains(event.target as Node)) {
        onClick(event);
      }
    };
  
    document.addEventListener('click', handleClick, true);
  
    return {
      destroy() {
        document.removeEventListener('click', handleClick, true);
      },
    };
  }

  return outsideClick;
}
