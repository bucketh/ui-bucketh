/* eslint-disable @typescript-eslint/ban-ts-comment */
import { readable } from 'svelte/store';

// NOTE: This doesn't work on mobile devices, only desktop :v

const mobileKeyboardVisible = readable(false, (set) => {
  if (typeof navigator === 'undefined') return;
  // @ts-ignore
  if (!navigator?.virtualKeyboard?.addEventListener) return;
  // @ts-ignore
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  navigator.virtualKeyboard.addEventListener('visibilitychange', (event: any) => {
    set(event.virtualKeyboardVisible);
  });
});

export default mobileKeyboardVisible;
