import type { MenuPosition, MenuPositionConfig } from './Menu.types';

function canFitOnLeft(anchor: DOMRect, menu: DOMRect) {
  return anchor.left - menu.width >= 0;
}
function canFitInsideLeft(anchor: DOMRect, menu: DOMRect) {
  return anchor.right - menu.width >= 0;
}
function canFitOnRight(anchor: DOMRect, menu: DOMRect) {
  return anchor.right + menu.width <= window.innerWidth;
}
function canFitInsideRight(anchor: DOMRect, menu: DOMRect) {
  return anchor.left + menu.width <= window.innerWidth;
}
function canFitOnTop(anchor: DOMRect, menu: DOMRect) {
  return anchor.top - menu.height >= 0;
}
function canFitInsideTop(anchor: DOMRect, menu: DOMRect) {
  return anchor.bottom - menu.height >= 0;
}
function canFitOnBottom(anchor: DOMRect, menu: DOMRect) {
  return anchor.bottom + menu.height <= window.innerHeight;
}
function canFitInsideBottom(anchor: DOMRect, menu: DOMRect) {
  return anchor.top + menu.height <= window.innerHeight;
}
function canFitCenterHorizontally(anchor: DOMRect, menu: DOMRect) {
  const anchorCenter = anchor.left + anchor.width / 2;
  const menuHalf = menu.width / 2;
  return anchorCenter - menuHalf >= 0 && anchorCenter + menuHalf <= window.innerWidth;
}
function canFitCenterVertically(anchor: DOMRect, menu: DOMRect) {
  const anchorCenter = anchor.top + anchor.height / 2;
  const menuHalf = menu.height / 2;
  return anchorCenter - menuHalf >= 0 && anchorCenter + menuHalf <= window.innerHeight;
}

export type fitOptions = {
  menu: DOMRect;
  anchor: DOMRect;
  at: MenuPositionConfig;
};

export function canFit({ anchor, at, menu }: fitOptions): boolean {
  switch(at) {
    case 'top-left': return canFitOnTop(anchor, menu) && canFitInsideLeft(anchor, menu);
    case 'top-center': return canFitOnTop(anchor, menu) && canFitCenterHorizontally(anchor, menu);
    case 'top-right': return canFitOnTop(anchor, menu) && canFitInsideRight(anchor, menu);
    case 'right-top': return canFitOnRight(anchor, menu) && canFitInsideTop(anchor, menu);
    case 'right-center': return canFitOnRight(anchor, menu) && canFitCenterVertically(anchor, menu);
    case 'right-bottom': return canFitOnRight(anchor, menu) && canFitInsideBottom(anchor, menu);
    case 'bottom-right': return canFitOnBottom(anchor, menu) && canFitInsideRight(anchor, menu);
    case 'bottom-center': return canFitOnBottom(anchor, menu) && canFitCenterHorizontally(anchor, menu);
    case 'bottom-left': return canFitOnBottom(anchor, menu) && canFitInsideLeft(anchor, menu);
    case 'left-bottom': return canFitOnLeft(anchor, menu) && canFitInsideBottom(anchor, menu);
    case 'left-center': return canFitOnLeft(anchor, menu) && canFitCenterVertically(anchor, menu);
    case 'left-top': return canFitOnLeft(anchor, menu) && canFitInsideTop(anchor, menu);
    case 'auto': return true;
  }
}

export type CalcPositionOptions = {
  menuElement: HTMLElement;
  anchorElement: HTMLElement;
  forcedPosition: MenuPositionConfig;
  preferedPositions: [MenuPositionConfig, ...MenuPositionConfig[]];
};

export function getPositionName({
  menuElement,
  anchorElement,
  forcedPosition,
  preferedPositions,
}: CalcPositionOptions): MenuPositionConfig {
  if (forcedPosition !== 'auto') return forcedPosition;

  const anchorRect = anchorElement.getBoundingClientRect();
  const menuRect = menuElement.getBoundingClientRect();
  const fitPosition = preferedPositions.find((pos) =>
    canFit({ anchor: anchorRect, at: pos, menu: menuRect }),
  ) || preferedPositions.at(-1) as MenuPositionConfig;

  return fitPosition;
}

function getFreestPosition(anchor: DOMRect): MenuPosition {
  const spaceTop = { side: 'top' as const, space: anchor.top };
  const spaceBottom = { side: 'bottom' as const, space: window.innerHeight - anchor.bottom };
  const spaceLeft = { side: 'left' as const, space: anchor.left };
  const spaceRight = { side: 'right' as const, space: window.innerWidth - anchor.right };

  const biggestSpace = [spaceTop, spaceBottom, spaceLeft, spaceRight].sort((a, b) => b.space - a.space)[0];
  const biggestSpaceX = [spaceLeft, spaceRight].sort((a, b) => b.space - a.space)[0];
  const biggestSpaceY = [spaceTop, spaceBottom].sort((a, b) => b.space - a.space)[0];

  if (biggestSpace.side === 'left' || biggestSpace.side === 'right') return `${biggestSpace.side}-${biggestSpaceY.side}`;
  return `${biggestSpaceY.side}-${biggestSpaceX.side}`;
}

type GetPositionRectOptions = {
  position: MenuPositionConfig;
  anchor: DOMRect;
  menu: DOMRect;
};

export function getPositionRect({ anchor, menu, position }: GetPositionRectOptions): DOMRect {
  if (position === 'auto') return getPositionRect({
    anchor,
    menu,
    position: getFreestPosition(anchor),
  });

  const anchorCenterX = anchor.left + anchor.width / 2;
  const anchorCenterY = anchor.top + anchor.height / 2;
  const menuHalfWidth = menu.width / 2;
  const menuHalfHeight = menu.height / 2;

  switch(position) {
    case 'top-left': return new DOMRect(anchor.right - menu.width, anchor.top - menu.height, menu.width, menu.height);
    case 'top-center': return new DOMRect(anchorCenterX - menuHalfWidth, anchor.top - menu.height, menu.width, menu.height);
    case 'top-right': return new DOMRect(anchor.left, anchor.top - menu.height, menu.width, menu.height);

    case 'right-top': return new DOMRect(anchor.right, anchor.bottom - menu.height, menu.width, menu.height);
    case 'right-center': return new DOMRect(anchor.right, anchorCenterY - menuHalfHeight, menu.width, menu.height);
    case 'right-bottom': return new DOMRect(anchor.right, anchor.top, menu.width, menu.height);

    case 'bottom-right': return new DOMRect(anchor.left, anchor.bottom, menu.width, menu.height);
    case 'bottom-center': return new DOMRect(anchorCenterX - menuHalfWidth, anchor.bottom, menu.width, menu.height);
    case 'bottom-left': return new DOMRect(anchor.right - menu.width, anchor.bottom, menu.width, menu.height);

    case 'left-bottom': return new DOMRect(anchor.left - menu.width, anchor.top, menu.width, menu.height);
    case 'left-center': return new DOMRect(anchor.left - menu.width, anchorCenterY - menuHalfHeight, menu.width, menu.height);
    case 'left-top': return new DOMRect(anchor.left - menu.width, anchor.bottom - menu.height, menu.width, menu.height);
  }
}
