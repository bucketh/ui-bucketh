/**
 * Make color for tailwind config based on M3 colors
 * @param { import('../types.js').PalettesConfig } palettes
 */
export function getTailwindThemeColors(palettes) {
  const {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    'neutral': neutralPalette,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    'neutral-variant': neutralVariantPalette,
    ...accentPalettes
  } = palettes;

  // accent colors
  const accentColors = Object.keys(accentPalettes).reduce((result, name) => {
    return {
      ...result,
      [name]: `rgb(var(--${name}) / <alpha-value>)`,
      [`on-${name}`]: `rgb(var(--on-${name}) / <alpha-value>)`,
      [`${name}-container`]: `rgb(var(--${name}-container) / <alpha-value>)`,
      [`on-${name}-container`]: `rgb(var(--on-${name}-container) / <alpha-value>)`,
      [`inverse-${name}`]: `rgb(var(--inverse-${name}) / <alpha-value>)`,
      // fixed
      [`${name}-fixed`]: `rgb(var(--${name}-100) / <alpha-value>)`,
      [`${name}-fixed-dim`]: `rgb(var(--${name}-200) / <alpha-value>)`,
      [`on-${name}-fixed`]: `rgb(var(--${name}-900) / <alpha-value>)`,
      [`on-${name}-fixed-variant`]: `rgb(var(--${name}-700) / <alpha-value>)`,
    };
  }, {});

  return {
    ...accentColors,
    'surface-dim': 'rgb(var(--surface-dim) / <alpha-value>)',
    'surface': 'rgb(var(--surface) / <alpha-value>)',
    'surface-bright': 'rgb(var(--surface-bright) / <alpha-value>)',

    'surface-container-lowest': 'rgb(var(--surface-container-lowest) / <alpha-value>)',
    'surface-container-low': 'rgb(var(--surface-container-low) / <alpha-value>)',
    'surface-container': 'rgb(var(--surface-container) / <alpha-value>)',
    'surface-container-high': 'rgb(var(--surface-container-high) / <alpha-value>)',
    'surface-container-highest': 'rgb(var(--surface-container-highest) / <alpha-value>)',

    'on-surface': 'rgb(var(--on-surface) / <alpha-value>)',
    'on-surface-variant': 'rgb(var(--on-surface-variant) / <alpha-value>)',
    'outline': 'rgb(var(--outline) / <alpha-value>)',
    'outline-variant': 'rgb(var(--outline-variant) / <alpha-value>)',

    'inverse-surface': 'rgb(var(--inverse-surface) / <alpha-value>)',
    'inverse-on-surface': 'rgb(var(--inverse-on-surface) / <alpha-value>)',

    'scrim': 'rgb(var(--scrim) / <alpha-value>)',
    'shadow': 'rgb(var(--shadow) / <alpha-value>)',

    // dynamic colors taken from `color-[name]` util
    // 'dynamic': {
      // '0': 'rgb(var(--dynamic-0) / <alpha-value>)',
      // '10': 'rgb(var(--dynamic-10) / <alpha-value>)',
      // '50': 'rgb(var(--dynamic-50) / <alpha-value>)',
      // '100': 'rgb(var(--dynamic-100) / <alpha-value>)',
      // '200': 'rgb(var(--dynamic-200) / <alpha-value>)',
      // '300': 'rgb(var(--dynamic-300) / <alpha-value>)',
      // '400': 'rgb(var(--dynamic-400) / <alpha-value>)',
      // '500': 'rgb(var(--dynamic-500) / <alpha-value>)',
      // '600': 'rgb(var(--dynamic-600) / <alpha-value>)',
      // '700': 'rgb(var(--dynamic-700) / <alpha-value>)',
      // '800': 'rgb(var(--dynamic-800) / <alpha-value>)',
      // '900': 'rgb(var(--dynamic-900) / <alpha-value>)',
      // '1000': 'rgb(var(--dynamic-1000) / <alpha-value>)',
      // DEFAULT: 'rgb(var(--dynamic, var(--dynamic-fallback)) / <alpha-value>)',
    // },
    'dynamic': 'rgb(var(--dynamic, var(--dynamic-fallback)) / <alpha-value>)',
    'on-dynamic': 'rgb(var(--on-dynamic, var(--on-dynamic-fallback)) / <alpha-value>)',
    'dynamic-container': 'rgb(var(--dynamic-container, var(--dynamic-container-fallback)) / <alpha-value>)',
    'on-dynamic-container': 'rgb(var(--on-dynamic-container, var(--on-dynamic-container-fallback)) / <alpha-value>)',
    'inverse-dynamic': 'rgb(var(--inverse-dynamic, var(--inverse-dynamic-fallback)) / <alpha-value>)',
  };
}
