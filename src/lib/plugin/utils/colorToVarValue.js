import hexRgb from './hexRgb';

/**
 * @param { string } color 
 * @returns { string }
 */
export default function toneToVars(color) {
  const { red, green, blue } = hexRgb(color);

  return `${red} ${green} ${blue}`;
}
