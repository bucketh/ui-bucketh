import colors from 'tailwindcss/colors.js';

// In favor of m3 neutral
// export const neutral = {
//   0: '#FFFFFF',
//   10: '#fcfcfc',
//   ...colors.neutral,
//   1000: '#000000',
// };

/**
 * @type { import('./types.js').Palette }
 */
export const red = {
  0: '#FFFFFF',
  10: '#fffafa',
  ...colors.red,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const yellow = {
  0: '#FFFFFF',
  10: '#fffdfa',
  ...colors.yellow,
  1000: '#000000',
};

// in favor of green
// const emerald = {
//   0: '#FFFFFF',
//   10: '#fafffd',
//   ...colors.emerald,
//   1000: '#000000',
// };

/**
 * @type { import('./types.js').Palette }
 */
export const green = {
  0: '#FFFFFF',
  10: '#fafffd',
  ...colors.green,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const blue = {
  0: '#FFFFFF',
  10: '#fafcff',
  ...colors.blue,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const indigo = {
  0: '#FFFFFF',
  10: '#fafaff',
  ...colors.indigo,
  1000: '#000000',
};

// in favor of violet
// const purple = {
//   0: '#FFFFFF',
//   10: '#',
//   ...colors.purple,
//   1000: '#000000',
// };

/**
 * @type { import('./types.js').Palette }
 */
export const pink = {
  0: '#FFFFFF',
  10: '#fffafc',
  ...colors.pink,
  1000: '#000000',
};

// in favor of red
// const rose = {
//   0: '#FFFFFF',
//   10: '#',
//   ...colors.rose,
//   1000: '#000000',
// };

/**
 * @type { import('./types.js').Palette }
 */
export const fuchsia = {
  0: '#FFFFFF',
  10: '#fefaff',
  ...colors.fuchsia,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const violet = {
  0: '#FFFFFF',
  10: '#fcfaff',
  ...colors.violet,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const cyan = {
  0: '#FFFFFF',
  10: '#fafeff',
  ...colors.cyan,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const teal = {
  0: '#FFFFFF',
  10: '#fafefe',
  ...colors.teal,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const lime = {
  0: '#FFFFFF',
  10: '#fdfffa',
  ...colors.lime,
  1000: '#000000',
};

// in favor of yellow
// const amber = {
//   0: '#FFFFFF',
//   10: '#',
//   ...colors.amber,
//   1000: '#000000',
// };

/**
 * @type { import('./types.js').Palette }
 */
export const orange = {
  0: '#FFFFFF',
  10: '#fffcfa',
  ...colors.orange,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const sky = {
  0: '#FFFFFF',
  10: '#fafdff',
  ...colors.sky,
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const primary = {
  0: '#FFFFFF',
  10: '#FFFBFE',
  50: '#F6EDFF',
  100: '#EADDFF',
  200: '#D0BCFF',
  300: '#B69DF8',
  400: '#9A82DB',
  500: '#7F67BE',
  600: '#6750A4',
  700: '#4F378B',
  800: '#381E72',
  900: '#21005D',
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const secondary = {
  0: '#FFFFFF',
  10: '#FFFBFE',
  50: '#F6EDFF',
  100: '#E8DEF8',
  200: '#CCC2DC',
  300: '#B0A7C0',
  400: '#958DA5',
  500: '#7A7289',
  600: '#625B71',
  700: '#4A4458',
  800: '#332D41',
  900: '#1D192B',
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const tertiary = {
  0: '#FFFFFF',
  10: '#FFFBFA',
  50: '#FFECF1',
  100: '#FFD8E4',
  200: '#EFB8C8',
  300: '#D29DAC',
  400: '#B58392',
  500: '#986977',
  600: '#7D5260',
  700: '#633B48',
  800: '#492532',
  900: '#31111D',
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const error = {
  0: '#FFFFFF',
  10: '#FFFBF9',
  50: '#FCEEEE',
  100: '#F9DEDC',
  200: '#F2B8B5',
  300: '#EC928E',
  400: '#E46962',
  500: '#DC362E',
  600: '#B3261E',
  700: '#8C1D18',
  800: '#601410',
  900: '#410E0B',
  1000: '#000000',
};

/**
 * @type { import('./types.js').NeutralPalette }
 */
export const neutral = {
  0: '#FFFFFF',
  10: '#FFFBFE',
  20: '#FEF7FF',
  40: '#F7F2FA',
  50: '#F4EFF4',
  60: '#F3EDF7',
  80: '#ECE6F0',
  100: '#E6E1E5',
  130: '#DED8E1',
  200: '#C9C5CA',
  300: '#AEAAAE',
  400: '#939094',
  500: '#787579',
  600: '#605D62',
  700: '#484649',
  760: '#3B383E',
  780: '#36343B',
  800: '#313033',
  830: '#2B2930',
  880: '#211F26',
  900: '#1C1B1F',
  940: '#141218',
  960: '#0F0D13',
  1000: '#000000',
};

/**
 * @type { import('./types.js').Palette }
 */
export const neutralVariant = {
  0: '#FFFFFF',
  10: '#FFFBFE',
  50: '#F5EEFA',
  100: '#E7E0EC',
  200: '#CAC4D0',
  300: '#AEA9B4',
  400: '#938F99',
  500: '#79747E',
  600: '#605D66',
  700: '#49454F',
  800: '#322F37',
  900: '#1D1A22',
  1000: '#000000',
};

/**
 * @type { import('./types.js').PalettesConfig }
 */
export const minimalPaletes = {
  'primary': primary,
  'secondary': secondary,
  'tertiary': tertiary,
  'error': error,
  'neutral': neutral,
  'neutral-variant': neutralVariant,
};

/**
 * @type { import('./types.js').PalettesConfig }
 */
export const extendedPalettes = {
  ...minimalPaletes,
  'red': red,
  'yellow': yellow,
  'green': green,
  'blue': blue,
  'indigo': indigo,
  'pink': pink,
  'fuchsia': fuchsia,
  'violet': violet,
  'cyan': cyan,
  'teal': teal,
  'lime': lime,
  'orange': orange,
  'sky': sky,
}
