/**
 * `.light-theme`: Configure token vars for light theme
 * 
 * `.dark-theme`: Configure token vars for dark theme

 * @param { import('../types.js').PalettesConfig } palettes 
 * @param { import('tailwindcss/types/config.js').PluginAPI } plugin
 */
export default function tokensUtilities(palettes, { addUtilities }) {
  const {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    neutral,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    'neutral-variant': neutralVariant,
    ...accentPalettes
  } = palettes;

  const accents = [...Object.keys(accentPalettes), 'dynamic'];

  addUtilities({
    '.light-theme': {
      // accents
      '*': {
        ...accents.reduce((result, name) => ({
          ...result,
          [`--${name}`]: `var(--${name}-600)`,
          [`--on-${name}`]: `var(--${name}-0)`,
          [`--${name}-container`]: `var(--${name}-100)`,
          [`--on-${name}-container`]: `var(--${name}-900)`,
          [`--inverse-${name}`]: `var(--${name}-200)`,
        }), {}),
      },

      // neutrals
      '--surface-dim': 'var(--neutral-130)', // 87
      '--surface': 'var(--neutral-20)', // 98
      '--surface-bright': 'var(--neutral-20)', // 98

      '--surface-container-lowest': 'var(--neutral-0)', // 100
      '--surface-container-low': 'var(--neutral-40)', // 96
      '--surface-container': 'var(--neutral-60)', // 94
      '--surface-container-high': 'var(--neutral-80)', // 92
      '--surface-container-highest': 'var(--neutral-100)', // 90

      '--on-surface': 'var(--neutral-900)', // 10
      '--on-surface-variant': 'var(--neutral-variant-700)', // 30
      '--outline': 'var(--neutral-variant-500)', // 50
      '--outline-variant': 'var(--neutral-variant-200)', // 80

      '--inverse-surface': 'var(--neutral-800)', // 20
      '--inverse-on-surface': 'var(--neutral-50)', // 95

      // // elevation surface
      // '--surface-background': 'var(--neutral-10)',
      // '--surface-overlay': 'var(--primary-600)',

      // // elevation shadow
      // '--elevation-1': '0px 1px 2px rgba(0, 0, 0, 0.3), 0px 1px 3px rgba(0, 0, 0, 0.15)',
      // '--elevation-2': '0px 1px 2px rgba(0, 0, 0, 0.3), 0px 2px 6px 2px rgba(0, 0, 0, 0.15)',
      // '--elevation-3': '0px 4px 8px 3px rgba(0, 0, 0, 0.15), 0px 1px 3px rgba(0, 0, 0, 0.3)',
      // '--elevation-4': '0px 6px 10px 4px rgba(0, 0, 0, 0.15), 0px 2px 3px rgba(0, 0, 0, 0.3)',
      // '--elevation-5': '0px 8px 12px 6px rgba(0, 0, 0, 0.15), 0px 4px 4px rgba(0, 0, 0, 0.3)',
    },
    '.dark-theme': {
      // accents
      '*': {
        ...accents.reduce((result, name) => ({
          ...result,
          [`--${name}`]: `var(--${name}-200)`,
          [`--on-${name}`]: `var(--${name}-800)`,
          [`--${name}-container`]: `var(--${name}-700)`,
          [`--on-${name}-container`]: `var(--${name}-100)`,
          [`--inverse-${name}`]: `var(--${name}-600)`,
        }), {}),
      },

      // neutrals
      '--surface-dim': 'var(--neutral-940)', // 6
      '--surface': 'var(--neutral-940)', // 6
      '--surface-bright': 'var(--neutral-760)', // 24

      '--surface-container-lowest': 'var(--neutral-960)', // 4
      '--surface-container-low': 'var(--neutral-900)', // 10
      '--surface-container': 'var(--neutral-880)', // 12
      '--surface-container-high': 'var(--neutral-830)', // 17
      '--surface-container-highest': 'var(--neutral-780)', // 22

      '--on-surface': 'var(--neutral-100)', // 90
      '--on-surface-variant': 'var(--neutral-variant-200)', // 80
      '--outline': 'var(--neutral-variant-400)', // 60
      '--outline-variant': 'var(--neutral-variant-700)', // 30

      '--inverse-surface': 'var(--neutral-100)', // 90
      '--inverse-on-surface': 'var(--neutral-800)', // 20

      '--scrim': 'var(--neutral-1000)', // 0
      '--shadow': 'var(--neutral-1000)',  // 0

      // // elevation surface
      // '--surface-background': 'var(--neutral-900)',
      // '--surface-overlay': 'var(--primary-200)',

      // // elevation shadow
      // '--elevation-1': '0px 1px 3px rgba(0, 0, 0, 0.15), 0px 1px 2px rgba(0, 0, 0, 0.3)',
      // '--elevation-2': '0px 2px 6px 2px rgba(0, 0, 0, 0.15), 0px 1px 2px rgba(0, 0, 0, 0.3)',
      // '--elevation-3': '0px 4px 8px 3px rgba(0, 0, 0, 0.15), 0px 1px 3px rgba(0, 0, 0, 0.3)',
      // '--elevation-4': '0px 6px 10px 4px rgba(0, 0, 0, 0.15), 0px 2px 3px rgba(0, 0, 0, 0.3)',
      // '--elevation-5': '0px 8px 12px 6px rgba(0, 0, 0, 0.15), 0px 4px 4px rgba(0, 0, 0, 0.3)',
    },
  });
}
