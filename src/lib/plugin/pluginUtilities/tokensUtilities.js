import colorToVarValue from '../utils/colorToVarValue.js';

/**
 * @param { string } paletteName 
 * @param { import('../types.js').Palette } tones
 */
function paletteVars(paletteName, tones) {
  // return {
  //   [`--${paletteName}-0`]: colorToVarValue(tones[0]),
  //   [`--${paletteName}-10`]: colorToVarValue(tones[10]),
  //   [`--${paletteName}-50`]: colorToVarValue(tones[50]),
  //   [`--${paletteName}-100`]: colorToVarValue(tones[100]),
  //   [`--${paletteName}-200`]: colorToVarValue(tones[200]),
  //   [`--${paletteName}-300`]: colorToVarValue(tones[300]),
  //   [`--${paletteName}-400`]: colorToVarValue(tones[400]),
  //   [`--${paletteName}-500`]: colorToVarValue(tones[500]),
  //   [`--${paletteName}-600`]: colorToVarValue(tones[600]),
  //   [`--${paletteName}-700`]: colorToVarValue(tones[700]),
  //   [`--${paletteName}-800`]: colorToVarValue(tones[800]),
  //   [`--${paletteName}-900`]: colorToVarValue(tones[900]),
  //   [`--${paletteName}-1000`]: colorToVarValue(tones[1000]),
  // };
  return Object.entries(tones).reduce((acc, [toneKey, toneValue]) => ({
    ...acc,
    [`--${paletteName}-${toneKey}`]: colorToVarValue(toneValue),
  }), {});
}

/**
 * `.tokens`: Set all tokens as variables

 * @param { import('../types.js').PalettesConfig } palettes 
 * @param { import('tailwindcss/types/config.js').PluginAPI } plugin
 */
export default function tokensUtilities(palettes, { addUtilities }) {
  const peletteVars = Object.entries(palettes).reduce((result, [name, tones]) => ({
    ...result,
    ...paletteVars(name, tones),
  }), {});

  addUtilities({
    '.tokens': {
      ...peletteVars,
      '--scrim': 'var(--neutral-1000)', // 0
      '--shadow': 'var(--neutral-1000)',  // 0
    },
  });
}
