import toneToVars from '../utils/colorToVarValue.js';

/**
 * Create utilities for `color-primary`, `color-secondary`, `color-tertiary` or any other color
 * @param { import('../types.js').PalettesConfig } palettes 
 * @param { import('tailwindcss/types/config.js').PluginAPI } plugin
 */
export default function colorUtilities(palettes, { addUtilities }) {
  const colors = Object.entries(palettes).reduce((result, [name, tones]) => ({
    ...result,
    [`.color-${name}`]: {
      '--dynamic-0': toneToVars(tones['0']),
      '--dynamic-10': toneToVars(tones['10']),
      '--dynamic-50': toneToVars(tones['50']),
      '--dynamic-100': toneToVars(tones['100']),
      '--dynamic-200': toneToVars(tones['200']),
      '--dynamic-300': toneToVars(tones['300']),
      '--dynamic-400': toneToVars(tones['400']),
      '--dynamic-500': toneToVars(tones['500']),
      '--dynamic-600': toneToVars(tones['600']),
      '--dynamic-700': toneToVars(tones['700']),
      '--dynamic-800': toneToVars(tones['800']),
      '--dynamic-900': toneToVars(tones['900']),
      '--dynamic-1000': toneToVars(tones['1000']),
    },
  }), {});

  addUtilities(colors);
}
