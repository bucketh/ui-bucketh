/**
 * Create extra utilities
 * @param { import('tailwindcss/types/config.js').PluginAPI } plugin
 */

export default function components(plugin) {
  plugin.addComponents({
    '.scrim': {
      '@apply fixed inset-0 bg-black bg-opacity-[32%] z-50': {},
    },
  });
}
