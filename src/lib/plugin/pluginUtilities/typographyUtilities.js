/**
  This function add the next utilities
 
 * @param { import('tailwindcss/types/config.js').PluginAPI } plugin
 */
export default function typographyUtilities({ addUtilities }) {
  addUtilities({
    '.text-display-large': {
      // 'font-family': 'Roboto',
      'font-size': '3.5625rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '4rem',
      'letter-spacing': '-0.01563rem',
    },
    '.text-display-medium': {
      // 'font-family': 'Roboto',
      'font-size': '2.8125rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '3.25rem',
    },
    '.text-display-small': {
      // 'font-family': 'Roboto',
      'font-size': '2.25rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '2.75rem',
    },
    '.text-headline-large': {
      // 'font-family': 'Roboto',
      'font-size': '2rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '2.5rem',
    },
    '.text-headline-medium': {
      // 'font-family': 'Roboto',
      'font-size': '1.75rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '2.25rem',
    },
    '.text-headline-small': {
      // 'font-family': 'Roboto',
      'font-size': '1.5rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '2rem',
    },
    '.text-title-large': {
      // 'font-family': 'Roboto',
      'font-size': '1.375rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '1.75rem',
    },
    '.text-title-medium': {
      // 'font-family': 'Roboto',
      'font-size': '1rem',
      'font-style': 'normal',
      'font-weight': '500',
      'line-height': '1.5rem',
      'letter-spacing': '0.00938rem',
    },
    '.text-title-small': {
      // 'font-family': 'Roboto',
      'font-size': '0.875rem',
      'font-style': 'normal',
      'font-weight': '500',
      'line-height': '1.25rem',
      'letter-spacing': '0.00625rem',
    },
    '.text-label-large': {
      // 'font-family': 'Roboto',
      'font-size': '0.875rem',
      'font-style': 'normal',
      'font-weight': '500',
      'line-height': '1.25rem',
      'letter-spacing': '0.00625rem',
    },
    '.text-label-medium': {
      // 'font-family': 'Roboto',
      'font-size': '0.75rem',
      'font-style': 'normal',
      'font-weight': '500',
      'line-height': '1rem',
      'letter-spacing': '0.03125rem',
    },
    '.text-label-small': {
      // 'font-family': 'Roboto',
      'font-size': '0.6875rem',
      'font-style': 'normal',
      'font-weight': '500',
      'line-height': '1rem',
      'letter-spacing': '0.03125rem',
    },
    '.text-body-large': {
      // 'font-family': 'Roboto',
      'font-size': '1rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '1.5rem',
      'letter-spacing': '0.03125rem',
    },
    '.text-body-medium': {
      // 'font-family': 'Roboto',
      'font-size': '0.875rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '1.25rem',
      'letter-spacing': '0.01563rem',
    },
    '.text-body-small': {
      // 'font-family': 'Roboto',
      'font-size': '0.75rem',
      'font-style': 'normal',
      'font-weight': '400',
      'line-height': '1rem',
    },
  });
}
