export interface TailwindColor {
  // md color: 95
  50: string;
  // md color: 90
  100: string;
  // md color: 80
  200: string;
  // md color: 70
  300: string;
  // md color: 60
  400: string;
  // md color: 50
  500: string;
  // md color: 40
  600: string;
  // md color: 30
  700: string;
  // md color: 20
  800: string;
  // md color: 10
  900: string;
}

export interface Palette extends TailwindColor {
  // md color: 100
  0: string;
  // md color: 99
  10: string;
  // md color: 0
  1000: string;
}

export interface NeutralPalette extends Palette {
  // md color: 98 
  20: string;
  // md color: 94
  40: string;
  // md color: 60
  60: string;
  // md color: 92
  80: string;
  // md color: 87
  130: string;

  // md color: 24
  760: string;
  // md color: 22
  780: string
  // md color: 17
  830: string;
  // md color: 12
  880: string;
  // md color: 6
  940: string;
  // md color: 4
  960: string;
}

export interface PalettesConfig {
  primary: Palette;
  secondary: Palette;
  tertiary: Palette;
  error: Palette;
  neutral: NeutralPalette;
  'neutral-variant': Palette;
  [name: string]: Palette;
}

export interface BucketPluginConfig {
  palettes?: PalettesConfig;
}
